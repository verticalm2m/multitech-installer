# MultiTech Conduit TTN Installer

The detailed instructions are available at the [TTN documentation](https://www.thethingsnetwork.org/docs/current/multitech/).

Installer to use:

- PROD: https://bitbucket.org/verticalm2m/multitech-installer/raw/master/installer.sh
- Preprod: https://bitbucket.org/verticalm2m/multitech-installer/raw/master/installer-pre.sh
- Naxoo: https://bitbucket.org/verticalm2m/multitech-installer/raw/master/installer-naxoo.sh